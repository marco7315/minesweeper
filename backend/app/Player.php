<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    protected $fillable = ['username'];

    public function scores()
    {
        return $this->hasMany(Score::class, 'player_id');
    }
}
