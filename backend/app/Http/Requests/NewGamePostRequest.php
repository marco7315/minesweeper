<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewGamePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rows' => 'required|numeric|min:8|max:12',
            'cols' => 'required|numeric|min:8|max:12',
            'username' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'rows.required' => 'El campo número de filas es obligatorio',
            'rows.numeric' => 'El campo número de filas debe ser entre 8 y 12',
            'rows.min' => 'El campo número de filas debe ser entre 8 y 12',
            'rows.max' => 'El campo número de filas debe ser entre 8 y 12',
            'cols.required' => 'El campo número de columnas es obligatorio',
            'cols.min' => 'El campo número de columnas debe ser entre 8 y 12',
            'cols.max' => 'El campo número de columnas debe ser entre 8 y 12',
            'username.required' => 'El campo username es obligatorio'
        ];
    }
}
