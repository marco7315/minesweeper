<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewGamePostRequest;
use App\Http\Requests\SaveScorePostRequest;
use App\Player;
use App\Score;

class MinesweeperController extends Controller
{
    public function newGame(NewGamePostRequest $request){
        
        $cols = $request->cols;
        $rows = $request->rows;
        $username_req = $request->username;

        $username = Player::where('username', $username_req)->first();

        if(!$username)
            $username = Player::create($request->all());

        //fake grid
        $total = $cols * $rows;
        $fake_grid = [];
        for ($i=0; $i < $total; $i++) { 
            $fake_grid[$i] = [
                'id' => $i,
                'number' => true,
                'bomb' => false
            ];
        }
        
        return response()->json([
            'msg' => 'new Game',
            'cols' => $cols,
            'rows' => $rows,            
            'grid' => $fake_grid,
            'newGame' => false,
            'isWinner' => false,
            'username' => $username,
        ], 200);
    }

    public function saveScore(SaveScorePostRequest $request){

        $player = $request->username;
        $time = $request->time;

        $player = Player::findOrFail($player['id']);
        $player->scores()->save(new Score(['time' => $time]));

        return response()->json([
            'msg' => 'save Score'
        ], 200);
    }

    public function bestScores(Request $request){

        $best_scores = Score::with('player')
                        ->orderByRaw('CONVERT(time, SIGNED) desc')
                        ->take(5)->get();

        return response()->json([
            'msg' => 'best Scores',
            'best_scores' => $best_scores
        ], 200);
    }
}