<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    use HasFactory;

    protected $fillable = ['time'];

    public function player()
    {
        return $this->belongsTo(Player::class, 'player_id');
    }
}
