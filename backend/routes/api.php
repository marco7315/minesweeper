<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MinesweeperController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('minesweeper')->group(function () {
    Route::post('/new-game', [MinesweeperController::class, 'newGame']);
    Route::post('/save-score', [MinesweeperController::class, 'saveScore']);
    Route::get('/best-scores', [MinesweeperController::class, 'bestScores']);
});

/*
    new game {
        "name": "game1",
        "rows": 10,
        "cols": 10,
        "mines": 10,
        "dificult": "easy|medium|dificult"
        "username": "tomasito"
    }

    save-score {
        "name": "game1",
        "score": 10292
    }

    best-scores {
        "user",
        "score"
        "time"
    }

    your-scores {
        "score",
        "time"
    }

*/
