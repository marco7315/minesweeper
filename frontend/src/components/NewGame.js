import React, { useState } from 'react';
import { Row, Col, FloatingLabel, Form, Button } from 'react-bootstrap';
import { fetchRequest } from '../helpers/api';
import Loader from './Loader';

const initForm = {
    username: '',
    rows: '',
    cols:''
};

export default function NewGame({ setState }) {

    const [loader, setLoader] = useState(false);
    const [form, setForm] = useState(initForm);
    const [errores, setErrores] = useState({});

    const handleChange = ({ target }) => {     
        setForm(prevForm => ({
            ...prevForm,
            [target.name] : target.value
        }));
    }

    const handleSubmit = e => {
        e.preventDefault();

        setErrores({});
        setLoader(true);
        
        fetchRequest('/minesweeper/new-game', form, 'POST').then(({ data }) => {
            setLoader(false);
            setState(data);            
        }).catch(error => {
            if (error.response && error.response.status === 422) {
                const { errors } = error.response.data;
                setErrores(errors);
            }
            setLoader(false);
        });
    }

    if(loader) return <Loader />

    return (
        <form onSubmit={ handleSubmit } style={{ marginTop: 20 }}>
            <Row className="g-2">
                <Col md>
                    <FloatingLabel controlId="floatingInputGrid" label="Nombre de jugador">
                    <Form.Control onChange={ handleChange } name="username" value={ form.username } type="text" placeholder="" />
                    </FloatingLabel>
                    {
                        errores.username ? (
                            <span style={{color: '#bf3838'}}>{errores.username[0]}</span>
                        ) : null
                    }
                </Col>
                <Col md>
                    <FloatingLabel controlId="floatingInputGrid" label="Filas">
                    <Form.Control onChange={ handleChange } name="rows" value={ form.rows } type="text" placeholder="" />
                    </FloatingLabel>
                    {
                        errores.rows ? (
                            <span style={{color: '#bf3838'}}>{errores.rows[0]}</span>
                        ) : null
                    }
                </Col>
                <Col md>
                    <FloatingLabel controlId="floatingInputGrid" label="Columnas">
                    <Form.Control onChange={ handleChange } name="cols" value={ form.cols } type="text" placeholder="" />
                    </FloatingLabel>
                    {
                        errores.cols ? (
                            <span style={{color: '#bf3838'}}>{errores.cols[0]}</span>
                        ) : null
                    }
                </Col>
            </Row>
            <div className="d-grid gap-2" style={{marginTop: 20}}>
                <Button variant="primary" type="submit">
                    Jugar
                </Button>
            </div>
        </form>
    )
}
