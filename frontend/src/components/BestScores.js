import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import { fetchRequest } from '../helpers/api';

export default function BestScores() {

    const [bestScores, setBestScores] = useState([]);

    useEffect(() => {
        fetchRequest('/minesweeper/best-scores', {}, 'GET').then(({ data }) => {
            setBestScores(data.best_scores);
        }).catch(error => {
            console.log(error)
        });
    }, [])

    return (
        <div style={{ marginTop: 50 }}>
            <h3>Mejores puntuaciones </h3>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Jugador</th>
                    <th>Tiempo (seg)</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        bestScores.map(({ id, time, player }, index) => (
                            <tr key={ id }>
                                <td>{ index + 1 }</td>
                                <td>{ player.username }</td>
                                <td>{ time }</td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </div>
    )
}
