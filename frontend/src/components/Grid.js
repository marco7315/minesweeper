import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import '../assets/styles/minesweeper.css';
import { fetchRequest } from '../helpers/api';

export default function Grid({ state }) {

    const { cols, rows, grid, username } = state;
    const [timer, setTimer] = useState(0);
    const [ winner, setWinner] = useState(false);

    const gridStyle = {
        gridTemplateColumns: `repeat(${cols}, 40px)`,
        gridTemplateRows: `repeat(${rows}, 40px)`
    };

    const sendWinner = () => { 

        const formData = {
            username, 
            time: timer
        };
        
        fetchRequest('/minesweeper/save-score', formData, 'POST').then(({ data }) => {
            console.log( data );
            setWinner(true);
        }).catch(error => {
            console.log(error)
        });
    }

    useEffect(() => {
        const timer = setInterval(() => {
            if(!winner)
                setTimer(prevState => prevState + 1);
        }, 1000);
        return () => clearTimeout(timer);
    });

    return (
        <div className="minesweeper">
            <div className="d-grid gap-2" style={{marginBottom: 20}}>
                {
                    !winner ? (
                        <Button onClick={ sendWinner } variant="primary" type="submit">
                            Simular ganador
                        </Button>
                    ) : <p style={{ textAlign: 'center', backgroundColor: '#f9b36b' }}>¡Felicidades Ganaste!</p>
                }
            </div>
            <div style={{ textAlign: 'center', marginBottom: 20 }}>
                <span>Tiempo: { timer } seg</span>
            </div>
            <div className="minesweeper__wrap">
                <ul 
                    className="minesweeper__grid"
                    style={ gridStyle }
                >
                    {
                        grid.map(({ id }) => {
                            return (
                                <li key={ id } className="minesweeper__square"></li>
                            )
                        })
                    }
                </ul>
            </div>
        </div>
    )
}