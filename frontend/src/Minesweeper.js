import React, { useState } from 'react';

import { Container } from 'react-bootstrap';
import BestScores from './components/BestScores';
import Grid from './components/Grid';
import NewGame from './components/NewGame';
import Title from './components/Title';

const initState = {
    newGame: true,
    isWinner: false,
    cols: '',
    rows: '',
    username: '',
    grid: []
}

export default function Minesweeper() {

    const [state, setState] = useState(initState);

    return (
        <Container style={{padding: '30px 0px'}}>
            <Title />
            {
                state.newGame ? (
                    <>
                        <NewGame setState={ setState } />
                        <BestScores />
                    </>
                ) : <Grid state={ state } />
            }            
        </Container>
    )
}