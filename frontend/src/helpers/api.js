import axios from 'axios';

const baseURL = process.env.REACT_APP_API_URL;

const instance = axios.create({
    baseURL,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
});

export const fetchRequest = ( endpoint, data, method = 'GET' ) => {

    if( method === 'GET' ){
        return instance({
            method: 'GET',
            url: endpoint
        });
    }else{
        return instance({
            method,
            url: endpoint,
            data
        });
    }

}